# Bulk Email

This tool was written to quickly fill a business requirement of sending mass newsletter emails via a local SMTP server.

# Features

- Supports HTML emails by default
- Uses only .NET APIs (no external deps)
- Supports configurable batch processing (e.g Send 100 at a time in a list of 1000 emails)
- Load HTML email template outside of the exe

# TODO
- Use external config file to define SMTP information and batch limit