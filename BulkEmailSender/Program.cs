﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic.FileIO;

namespace BulkEmailSender
{
    partial class BulkEmail
    {
        //Global variables
        private string EmailResult;
        public int lastEmailCount;
        private static string[] fields;

        static void Main(string[] args)
        {
            //Call program.
            BulkEmail BulkEmail = new BulkEmail();

            //Get current executable folder.
            string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
            var directory = System.IO.Path.GetDirectoryName(path);

            //Load CSV File
            var csvFile = directory+"\\emaillist.csv";

            //If the lastemail count file exists, read it, otherwise create it with 0.
            if (File.Exists(directory+"\\lastemail.txt"))
            {
                string[] emailCount = File.ReadAllLines(directory+"\\lastemail.txt");
                BulkEmail.lastEmailCount = Convert.ToInt32(emailCount[0]);
            } else
            {
                File.WriteAllText(directory + "\\lastemail.txt", "0");
                string[] emailCount = File.ReadAllLines(directory + "\\lastemail.txt");
                BulkEmail.lastEmailCount = Convert.ToInt16(emailCount[0]);
            }

            //Parse the loaded CSV and set the delimmeter.
            using (TextFieldParser csvParser = new TextFieldParser(csvFile))
            {
                csvParser.CommentTokens = new string[] { "#" };
                csvParser.SetDelimiters(new string[] { "," });
                csvParser.HasFieldsEnclosedInQuotes = false;

                //We only want to send emails in bulks of 100, so set the new upper limit.
                int emailLimit = BulkEmail.lastEmailCount + 100;

                for (int i = 0; i < BulkEmail.lastEmailCount; i++)
                {
                    string[] fields = csvParser.ReadFields();
                }
                //Compare the amount of emails sent since last run, with the new upper limit.
                while (BulkEmail.lastEmailCount < emailLimit)
                {
                    try
                    {
                        fields = csvParser.ReadFields();
                        string Name = fields[0].ToString();
                        string Email = fields[2].ToString();

                        //Console.WriteLine(Email);
                        BulkEmail.SendEmail(Name, Email);
                        BulkEmail.lastEmailCount++;
                        File.WriteAllText(directory + "\\lastemail.txt", BulkEmail.lastEmailCount.ToString());
                    } catch
                    {
                        if (fields == null)
                        {
                            //End of file.
                            Environment.Exit(0);
                        }
                    }
                }
            }

            Console.Read();
        }
    }
}
