﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;
using System.IO;

namespace BulkEmailSender
{
    partial class BulkEmail
    {
        public void SendEmail(string name, string email)
        {
            string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
            var directory = System.IO.Path.GetDirectoryName(path);

            string bodyTemplate = File.ReadAllText(directory+"\\template.html");
            string body = bodyTemplate.Replace("{__FIRSTNAME__}", name);

            MailMessage emailconfig = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("");

            emailconfig.From = new MailAddress("");
            emailconfig.To.Add(email);
            emailconfig.Subject = @"";

            emailconfig.IsBodyHtml = true;
            emailconfig.Body = body;

            SmtpServer.Port = 25;
            SmtpServer.UseDefaultCredentials = false;
            SmtpServer.EnableSsl = false;
            SmtpServer.Send(emailconfig);

            Console.WriteLine("Email Sent to " + email);
            //return "OK";
        }
    }
}
